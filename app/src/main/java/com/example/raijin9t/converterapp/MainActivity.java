package com.example.raijin9t.converterapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Declare two buttons exit button and convertbutton
        Button convertButton = (Button) findViewById(R.id.convertButton);
        Button exitButton = (Button) findViewById(R.id.exitButton);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
            }
        });


        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText millimeter = (EditText) findViewById(R.id.mmEditText);
                EditText inches = (EditText) findViewById(R.id.inEditText);

                double result, mm;
                mm = Double.parseDouble(millimeter.getText().toString());
                result = (mm/25.4);

                inches.setText(Double.toString(result));
            }
        });


    }
}
